import React, { useEffect, useState } from 'react'

const Flame0 = ({ height, width }) => {
    return (
        <svg width={width} height={height} viewBox="0 0 125 125" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M85.5 46.5C88.4516 30.5166 85.4217 21.8609 57.0673 0C58.9249 20.0743 60.131 31.5863 48.1413 30.0176C44.4219 36.2551 42 47.315 42 56.5C42 65.685 27.513 72.0936 29 56.5C15.9826 72.8733 20.4454 86.8642 29.0001 97C37.5548 107.136 28.7994 109.155 26.5677 108.765C24.336 108.375 20.2446 105.647 20.2446 105.647C41.4456 131.766 74.5488 129.817 97.9814 109.545C97.9814 109.545 97.2375 109.935 95.3777 110.325C93.518 110.714 92.1405 108.99 95.3777 105.647C104 89.5 103.5 82 97.9814 56.5C97.9814 56.5 94.8948 59.3219 93.5 64C92.1052 68.6781 82.5484 62.4834 85.5 46.5Z" fill="url(#paint0_linear)" />
            <defs>
                <linearGradient id="paint0_linear" x1="20.2446" y1="125" x2="97.2598" y2="109.654" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#56CCF2" />
                    <stop offset="1" stopColor="#2F80ED" />
                </linearGradient>
            </defs>
        </svg>
    )
}

const Flame1 = ({ height, width }) => {
    return (
        <svg width={width} height={height} viewBox="0 0 125 125" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M88.4998 49C91.4998 43 88.4998 31 67.9998 7.5C65.4998 27 65.4998 33.5 52.9998 33.5C46.9998 33.5 38.4998 38.2567 35.9998 52.6283C33.4998 67 29.4998 68 25 63C17.5 80.5 15.9998 88 22.1044 97.8497C28.209 107.699 28.7994 109.155 26.5677 108.765C24.3361 108.375 20.2447 105.647 20.2447 105.647C41.4456 131.766 74.5488 129.817 97.9814 109.545C97.9814 109.545 97.2375 109.935 95.3778 110.325C93.518 110.714 88.9998 110.325 92.4998 105.647C95.9998 100.968 103 89.5 97.9814 65C97.9814 65 86.4998 70 83.9998 65C81.4998 60 85.4998 55 88.4998 49Z" fill="url(#paint0_linear)" />
            <defs>
                <linearGradient id="paint0_linear" x1="18.3182" y1="125" x2="94.6468" y2="108.89" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#56CCF2" />
                    <stop offset="1" stopColor="#2F80ED" />
                </linearGradient>
            </defs>
        </svg>

    )
}

const Flame2 = ({ height, width }) => {
    return (
        <svg width={width} height={height} viewBox="0 0 125 125" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M82.4998 8.5C72.4998 32 87.9999 34.5 75.9999 51C70.9999 61.5 70.4999 73.5968 75.9999 77.5C91.4998 88.5 95.3778 70 95.3778 70C100.396 94.5 95.9998 100.968 92.4998 105.647C88.9998 110.325 93.5181 110.714 95.3778 110.325C97.2375 109.935 97.9814 109.545 97.9814 109.545C75.1532 129.294 43.1469 131.653 21.9086 107.612C20.5524 106.724 20.2447 105.647 20.2447 105.647C20.7915 106.32 21.3463 106.975 21.9086 107.612C22.8602 108.235 24.3279 108.765 26.5678 108.765L26.7052 108.765C32.0108 108.766 32.6201 108.766 26.5674 99C20.4628 89.1503 14.6049 82.5 22.1049 65C26.6047 70 33.4998 62.8717 35.9998 48.5C38.4998 34.1283 43.1655 31.9002 48.9998 30.5C61.4998 27.5 54.4998 19.5 82.4998 8.5Z" fill="url(#paint0_linear)" />
            <defs>
                <linearGradient id="paint0_linear" x1="18.5717" y1="125" x2="93.122" y2="109.521" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#56CCF2" />
                    <stop offset="1" stopColor="#2F80ED" />
                </linearGradient>
            </defs>
        </svg>
    )
}
const Flame3 = ({ height, width }) => {
    return (
        <svg width={width} height={height} viewBox="0 0 125 126" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M61.5 16C51.5 39.5 76.4999 35.5 64.4999 52C59.4999 62.5 68.4999 72.5 79.9999 75C91.4999 77.5 98.4097 73.5 98.4097 73.5C95.5317 97.6465 96 98.8219 92.5 103.5C89 108.178 93.9464 111.214 95.8061 110.825C97.6658 110.435 98.4097 110.045 98.4097 110.045C75.5815 129.794 43.5752 132.153 22.3369 108.112C20.9807 107.224 20.673 106.147 20.673 106.147C21.2198 106.82 21.7746 107.475 22.3369 108.112C23.2885 108.735 24.7562 109.265 26.996 109.265C32.4282 109.265 30.9914 103 26.9957 99.5C23 96 16.9999 92 20.673 77.5C39.9999 85 24.9999 66 24.9999 53.5C24.9999 41 30.5 42 32.5 35.5C34.5 29 33.5 27 61.5 16Z" fill="url(#paint0_linear)" />
            <defs>
                <linearGradient id="paint0_linear" x1="19.5289" y1="125.5" x2="93.2265" y2="109.328" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#56CCF2" />
                    <stop offset="1" stopColor="#2F80ED" />
                </linearGradient>
            </defs>
        </svg>
    )
}
const Flame4 = ({ height, width }) => {
    return (
        <svg width={width} height={height} viewBox="0 0 125 126" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M46.4999 5.5C42.5 23.5 53.0001 42 60.0001 27.5C68.5001 39.5 83.5001 39 75.0001 52.5C66.5001 66 79.0001 66.5 91.5001 59C104.5 81.5 101.417 92.5 95.8062 100C90.1949 107.5 91 110.825 95.8062 110.825C96.5407 110.825 97.1116 110.806 97.5516 110.776C97.8391 110.535 98.1252 110.291 98.4098 110.045C98.4098 110.045 99.9905 110.604 97.5516 110.776C74.1747 130.36 41.6164 131.949 20.673 106.147C27.121 110.461 30.1018 110.635 34.5001 107.5C34.5001 107.5 22.0001 95 25.0001 78C32.5001 83.5 40.0001 73 34.5001 64.5C45.5001 66.5 29.004 52.5 25.0001 39.5C20.9963 26.5 33.4999 12.5 46.4999 5.5Z" fill="url(#paint0_linear)" />
            <defs>
                <linearGradient id="paint0_linear" x1="20.673" y1="125.5" x2="95.8838" y2="110.233" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#56CCF2" />
                    <stop offset="1" stopColor="#2F80ED" />
                </linearGradient>
            </defs>
        </svg>
    )
}
const Flame5 = ({ height, width }) => {
    return (
        <svg width={width} height={height} viewBox="0 0 125 126" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M39 4C49.0001 23 61.5001 20.5 68.5001 27C75.5001 33.5 64.5001 56.5 85.0001 41.5C91.9999 46.5 77.9999 76 96.1331 59C96.1331 81.5 102.5 85 98.7368 98C94.9736 111 91.327 111.325 96.1332 111.325C96.8678 111.325 97.4386 111.306 97.8786 111.276C98.1661 111.035 98.4522 110.791 98.7368 110.545C98.7368 110.545 100.317 111.104 97.8786 111.276C74.5017 130.86 41.9433 132.449 20.9999 106.647C27.448 110.961 31.0001 106.647 33.5001 96C27.9999 92 15.9998 72.5 21 55.5C30 65.5 39.8161 61.5 28 39C21.1729 26 39 19 39 4Z" fill="url(#paint0_linear)" />
            <defs>
                <linearGradient id="paint0_linear" x1="19.8078" y1="126" x2="95.2474" y2="110.909" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#56CCF2" />
                    <stop offset="1" stopColor="#2F80ED" />
                </linearGradient>
            </defs>
        </svg>
    )
}
const Flame6 = ({ height, width }) => {
    return (
        <svg width={width} height={height} viewBox="0 0 125 126" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M48.5 1.5C67.5 14.5 78.5 15.5 78.5 28C78.5 40.5 74 68.5 96.1333 54C96.1333 74.5 111.5 92.5 96.1333 100.5C80.7666 108.5 91.3271 111.325 96.1333 111.325C96.8679 111.325 97.4387 111.306 97.8787 111.276C98.1662 111.035 98.4523 110.791 98.7369 110.545C98.7369 110.545 100.318 111.104 97.8787 111.276C74.5018 130.86 41.9435 132.449 21.0001 106.647C27.4481 110.961 34.8272 104.5 34.8272 97C34.8272 97 9.50002 77.5 25.5001 61.5C41.7707 75.0013 34.8272 58 34.8272 51C34.8272 44 39.4473 38.7292 41.5 31.5C51 29.5 58 27.5 48.5 1.5Z" fill="url(#paint0_linear)" />
            <defs>
                <linearGradient id="paint0_linear" x1="20.1999" y1="126" x2="98.0555" y2="110.235" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#56CCF2" />
                    <stop offset="1" stopColor="#2F80ED" />
                </linearGradient>
            </defs>
        </svg>
    )
}

const components = {
    Flame0: Flame0,
    Flame1: Flame1,
    Flame2: Flame2,
    Flame3: Flame3,
    Flame4: Flame4,
    Flame5: Flame5,
    Flame6: Flame6
};

export const Loader = ({ height, width }) => {
    const [step, setStep] = useState(0);

    useEffect(() => {
        setTimeout(() => setStep((step) => (step + 1) % 7), 100)
    }, [step])

    const TagName = components[`Flame${step}`];

    return (<TagName height={height || 125} width={width || 125} />)
}