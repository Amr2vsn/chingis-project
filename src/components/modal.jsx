import React from 'react';

export const Modal = ({ show, closeModal, className, children}) => {
    return (
        <>
            {show && <div className="backdrop" onClick={closeModal}></div>}
            <div className={`modal flex-center bshadow bradius-5 b-default ${show && 'modal-animation'}`}>
                {children}
            </div>
        </>
    );
};