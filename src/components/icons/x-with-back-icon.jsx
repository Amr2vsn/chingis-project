import React from 'react'

export const XIconBack = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M14 24C19.5228 24 24 19.5228 24 14C24 8.47715 19.5228 4 14 4C8.47715 4 4 8.47715 4 14C4 19.5228 8.47715 24 14 24Z" fill="white" />
                <path d="M17 11L11 17M11 11L17 17M24 14C24 19.5228 19.5228 24 14 24C8.47715 24 4 19.5228 4 14C4 8.47715 8.47715 4 14 4C19.5228 4 24 8.47715 24 14Z" stroke="#52575C" strokeLinecap="round" strokeLinejoin="round" />
            </svg>

        </span>
    )
}