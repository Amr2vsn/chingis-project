import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Stack, Button } from '../components';
import { VerifiedIcon } from '../components/icons/verified-icon'
import { ArrowIcon } from '../components/icons/arrow-icon'
import { AuthContext } from '../providers/auth-user-provider'

export const Verify = () => {
    const history = useHistory();
    const { user } = useContext(AuthContext);
    let email;

    if (user) {
        email = user.email;
    }
    console.log(user)
    console.log(email)

    const sendVerification = () => {
        window.location.href = `https://mail.google.com/mail/${email}/0/#inbox`
    }

    return (
        <div className="h-vh-99">
            <Stack className="ml-18 mr-18 flex">
                <div className="mt-20 mb-10 bold flex-row justify-between flex-between">
                    <ArrowIcon onClick={() => { history.goBack() }} width={20} height={20} />
                    <div className=" flex-center bold relative fs-16 lh-22 font-main ">Verify Account</div>
                    <div className="w-20 h-20 b-default"></div>
                </div>
                <VerifiedIcon className="flex-center mb-30" width='70' height='70' />
                <div className="fs-16 bold lh-22 bold font-main flex-start">Та өөрийн компани хаягаа баталгаат хаяг bolgosnoor :</div>
                <div className="fs-16 ma-9 normal lh-22 font-main ">Hereglegchdiin huviin medeeleliig harah (Utasnii dugaar email hayg zereg Account)</div>
                <div className="fs-16 ma-9 normal lh-22 font-main ">Tender uusgeh erh (Handiviin mungun dungee oruulaad hereglegchdiin dund tender zarlan ursulduulen songoj avah bolomjtoi)</div>
                <div className="mt-10 fs-16 bold lh-22 font-main flex-start">Verified hayg bolgoh zaavar</div>
                <div className="fs-16 ma-9 normal lh-22 font-main ">Bidnii yuvuulsan email haygruu kompanii taniltsuulga bolon huviin medeeleliig yuvuulah</div>
                <div className="fs-16 ma-9 normal lh-22 font-main ">Bid tanii yuvuulsan medeeleltei taniltsan verified hayg bolgoh bolno.</div>
                <div className="fs-16 bold lh-22 font-main flex-start">Email : sonorm971@gmail.com</div>
                <Button onClick={sendVerification} className="bradius-5 mt-20 b-primary c-white lh-22 font-main fs-16">Mail ilgeeh</Button>
            </Stack>
        </div>
    )
}